﻿using System;
using System.Numerics;

namespace BusPassenger
{
    class Program
    {
        static void Main(string[] args)
        {
            // you can change the input as desired
            int[,] input = new int[,] { { 0, 3 }, { 2, 5 }, { 4, 2 }, { 4, 0 } };
            Console.WriteLine("Your Result is " + ComputeBusPassengers(input));
        }

        static int ComputeBusPassengers (int[,] inputForPassengers)
        {
            // static function and passed the input
            
            int result = 0;
            int numOfStops = inputForPassengers.GetLength(0);

            if (numOfStops == 0)
                return 0;

            else if (numOfStops > 0)
            {
                int passengerCount = 0;
               
                for (int i=0; i < numOfStops; i++)
                {
                    //stores the boarding and unboarding passengers for every stop
                    int passengerUnboardingCount = inputForPassengers[i, 0];
                    int passengerBoardingCount = inputForPassengers[i, 1];

                    passengerCount = passengerCount - passengerUnboardingCount + passengerBoardingCount;
                    // maximum passengerCount at any stop is stored in the result
                    if (passengerCount > result)
                        result = passengerCount;
                }
                return result;
            }
           
            return -1;
        }

    }
}
